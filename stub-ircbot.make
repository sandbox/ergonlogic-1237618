;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A stub drush makefile to create an up-to-date IRC Bot installation profile
; on a Pressflow core.

; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make stub-ircbot.make"
; or create a new platform by calling this makefile:
;   "/var/aegir/makefiles/stub-ircbot.make"
; using Aegir's web interface (node/add/platform).

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 7.x

;******************************************************************************
;                               Include files
;******************************************************************************

; get fast core and set version
projects[pressflow][type] = core
projects[pressflow][download][type] = git
projects[pressflow][download][url] = git://github.com/pressflow/7.git
;includes[pressflow] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob/HEAD:/core-pressflow-7.make
;projects[pressflow][download][branch] = master

project[ircbot][type] = profile
project[ircbot][download][type] = git
project[ircbot][download][url] = http://drupalcode.org/project/ircbot_profile.git
projects[ircbot][download][branch] = 7.x-1.x

;http://drupalcode.org/project/ircbot_profile.git/blob_plain/refs/heads/7.x-1.x:/ircbot_profile.make
;projects[ircbot][download][tag] = 6.x-1.0

;includes[drupal_modules_maj] = includes/drupal-6-modules-maj.make

;******************************************************************************
;                        Additional features (optional)
;******************************************************************************


;******************************************************************************
;                                     End
;******************************************************************************
