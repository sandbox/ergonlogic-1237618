;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A stub drush makefile to create an up-to-date OpenAtrium installation profile
; on a Pressflow core.

; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make openatrium-stub.make"
; or create a new platform by calling this makefile:
;   "/var/aegir/makefiles/openatrium.make"
; using Aegir's web interface (node/add/platform).

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;******************************************************************************
;                               Include files
;******************************************************************************

; get fast core and set version
includes[pressflow] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob_plain/HEAD:/core-pressflow.make
projects[pressflow][download][tag] = DRUPAL-6-22

includes[openatrium] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob_plain/HEAD:/profile-openatrium.make
projects[openatrium][download][tag] = 6.x-1.0

;includes[drupal_modules_maj] = includes/drupal-6-modules-maj.make

;******************************************************************************
;                        Additional features (optional)
;******************************************************************************
; Atrium Spreadsheets
; Atrium Bookmarks
; http://drupal.org/project/atrium_glossary
; http://drupal.org/project/atrium_leads
; https://github.com/nuvoleweb/atrium_folders
; http://drupal.org/project/oa_iterations
; http://drupal.org/sandbox/phishsauce/1219452
; http://drupal.org/project/meetings
; http://drupal.org/project/ideation
; http://drupal.org/project/casetracker_comment_driven


;******************************************************************************
;                                     End
;******************************************************************************
