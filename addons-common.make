;******************************************************************************
;                               Documentation
;******************************************************************************
;
; Description:
; A drush makefile to build the standard Features stack.

;******************************************************************************
;                                 General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;******************************************************************************
;                              Common Modules
;******************************************************************************

; Utilities
projects[captcha][destination] = profiles/launch/modules/contrib
projects[recaptcha][destination] = profiles/launch/modules/contrib
projects[pathauto][destination] = profiles/launch/modules/contrib
projects[token][destination] = profiles/launch/modules/contrib

;******************************************************************************
;                                  End
;******************************************************************************
