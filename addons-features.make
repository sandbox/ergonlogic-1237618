;*******************************************************************************
;                                 Documentation
;*******************************************************************************
;
; Description:
; A drush makefile to build the standard Features stack.

;*******************************************************************************
;                                    General
;*******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;*******************************************************************************
;                                 Features stack
;*******************************************************************************

; Core
projects[features][destination] = contrib
projects[strongarm][destination] = contrib
projects[ctools][destination] = contrib
projects[diff][destination] = contrib

; Extras
projects[features_uuid][destination] = contrib
projects[uuid][destination] = contrib
projects[context][destination] = contrib
projects[boxes][destination] = contrib
projects[exportables][destination] = contrib
; http://drupal.org/project/casetracker_comment_driven
; http://drupal.org/project/record_feature
; http://drupal.org/project/variable_changes

;******************************************************************************
;                                  End
;******************************************************************************
