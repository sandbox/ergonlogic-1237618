;****************************************
; Documentation
;****************************************

; Description:
; A drush makefile for the OpenOutreach profile.

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 7.x

;****************************************
; OpenOutreach profile
;****************************************

projects[openoutreach][type] = profile
projects[openoutreach][download][type] = git
projects[openoutreach][download][url] = git://drupalcode.org/project/openoutreach.git
;projects[openoutreach][download][branch] = 7.x-1.x
projects[openoutreach][download][tag] = 7.x-1.0-beta5

;****************************************
; End
;****************************************
