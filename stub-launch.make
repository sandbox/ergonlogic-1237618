;****************************************
; Documentation
;****************************************

; Description:
; A stub drush makefile for an out-of-the-box service launch survey site.

; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make stub-OpenAtria.make"
; or create a new platform by calling this makefile:
;   "/var/aegir/makefiles/OpenAtria.make"
; using Aegir's web interface (node/add/platform).

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;****************************************
; Include files
;****************************************

; get fast core and set version
;includes[pressflow] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob_plain/HEAD:/core-pressflow.make
;projects[pressflow][download][tag] = DRUPAL-6-22
includes[drupal] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob_plain/HEAD:/core-drupal.make

includes[launch] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob_plain/HEAD:/profile-launch.make
;projects[launch][download][tag] = 6.x-1.0

; TODO look into j0nathan's update tools
;includes[drupal_modules_maj] = includes/drupal-6-modules-maj.make

;****************************************
; End
;****************************************
