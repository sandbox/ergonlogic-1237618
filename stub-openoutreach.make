;******************************************************************************
;                                Documentation
;******************************************************************************

; Description:
; A stub drush makefile to create an up-to-date OpenOutreach installation
; profile on a Pressflow core.

; Instructions:
; In your platforms directory (/var/aegir/platforms), run:
;   "drush make stub-openoutreach.make"
; or create a new platform by calling this makefile:
;   "/var/aegir/makefiles/stub-openoutreach.make"
; using Aegir's web interface (node/add/platform).

;******************************************************************************
;                                   General
;******************************************************************************

; drush make API version
api = 2

; Drupal core
core = 7.x

;******************************************************************************
;                               Include files
;******************************************************************************

; get fast core and set version
includes[pressflow] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob_plain/HEAD:/core-pressflow-7.make
;projects[pressflow][download][tag] = DRUPAL-7-9

includes[openoutreach] = http://drupalcode.org/sandbox/ergonlogic/1237618.git/blob_plain/HEAD:/profile-openoutreach.make

;includes[drupal_modules_maj] = includes/drupal-6-modules-maj.make

;******************************************************************************
;                                     End
;******************************************************************************
