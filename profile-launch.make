;****************************************
; Documentation
;****************************************

; Description:
; A drush makefile for a Launch profile.

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;****************************************
; Launch profile
;****************************************

projects[launch][type] = profile
projects[launch][download][type] = git
projects[launch][download][url] = git://git.drupal.org/sandbox/ergonlogic/1241176.git

;****************************************
; Modules to overwrite version
;****************************************

;projects[devel][destination] = profiles/openatrium/modules/developer
;projects[devel][version] = 1.25

;****************************************
; End
;****************************************
