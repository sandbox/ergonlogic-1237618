;****************************************
; Documentation
;****************************************

; Description:
; A drush makefile for Pressflow core.

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;****************************************
; Drupal 6.x core
;****************************************

projects[drupal][type] = core

; Patch from http://drupal.org/node/883038#comment-4554420
projects[drupal][patch][] = http://drupal.org/files/issues/ereg-suppress_warnings-883038-27.patch

;****************************************
; Patches
;****************************************

; Improvements to the core robots.txt file.
; https://redmine.koumbit.net/issues/2465
;projects[pressflow][patch][] = https://redmine.koumbit.net/projects/maj/repository/revisions/master/raw/patches/drupal-6.22/robots.txt.patch

; Empty $account->roles causes a sql error in user_access
; http://drupal.org/node/777116
;projects[pressflow][patch][] = http://drupal.org/files/issues/777116-no-roles-error-D6.patch

;****************************************
; End
;****************************************
