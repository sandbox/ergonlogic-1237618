;****************************************
; Documentation
;****************************************

; Description:
; A drush makefile for OpenEnterprise profile.

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 6.x

;****************************************
; OpenEnterprise profile
;****************************************

projects[openenterprise][type] = profile
projects[openenterprise][download][type] = git
projects[openenterprise][download][url] = git://drupalcode.org/project/openenterprise.git
projects[openenterprise][download][branch] = master

;****************************************
; End
;****************************************
